"""This script has code to generate the dataframes that will be used in the notebook script. You need to run this script before generating the notebook
You need to run the TRR_utilities cells in the interactive window first
The first import is just to get pylance to recognize the functions and be able to switch to them with Cmd+click
"""

from .helper_functions_pandas import (
    calc_avgs_per_testgroup_home,
    calc_avgs_per_testgroup_new_format_watched,
    calc_metrics_samples,
    create_results_df,
    change_legend_names,
)  # noqa ignore=F405

# %%
import pandas as pd
import awswrangler as wr
import plotly.io as pio
import boto3


pio.templates.default = "plotly_white"

# %%
bucket = "dev-data-science-lab"
path = "recos/your_personal_suggestions/AB_Testing/HybridSVD_vs_ALS"

# control_color = 'rgb(148, 195, 209, 0.5)'
# test_color = 'rgb(219, 121, 216, 0.5)'
control_color = "rgb(0, 134, 154, 0.5)"
test_color = "rgb(229, 0, 100, 0.5)"

boto3_session = boto3.session.Session(profile_name="dev")
# %%
# Load DPE data
# These are small files - no need to save locally
dfp_kpis = wr.s3.read_parquet(
    path=f"s3://{bucket}/{path}/pre_agg_data/kpis_dpe_user.parquet",
    boto3_session=boto3_session,
)
dfp_kpis = change_legend_names(dfp_kpis)

# %%
dfp_kpis_plat = wr.s3.read_parquet(
    f"s3://{bucket}/{path}/pre_agg_data/kpis_dpe_user_plat.parquet",
    boto3_session=boto3_session,
)
dfp_kpis_plat = change_legend_names(dfp_kpis_plat)
# remove_hbbtv
dfp_kpis_plat = dfp_kpis_plat[dfp_kpis_plat.platform != "ga-hbbtv"]
# %%
dfp_kpis_home = wr.s3.read_parquet(
    f"s3://{bucket}/{path}/pre_agg_data/kpis_home_user.parquet",
    boto3_session=boto3_session,
)
dfp_kpis_home = change_legend_names(dfp_kpis_home)

# keep only users with usage of DPE
dfp_kpis_home_filter = dfp_kpis_home.merge(
    dfp_kpis[["profile_id"]], on="profile_id", how="inner"
)
# %%
# ************************** DPE teaser row *******************

# calculate sample statistics
samples_control = calc_metrics_samples(
    df=dfp_kpis[dfp_kpis.group == "control"],
    n=1000,
    metrics_function=calc_avgs_per_testgroup_dpe,
    fraction=0.111,  # so that samples have the same size of test group
)

samples_test = calc_metrics_samples(
    df=dfp_kpis[dfp_kpis.group == "test"],
    n=1000,
    metrics_function=calc_avgs_per_testgroup_dpe,
    fraction=1.0,
    with_replacement=True,
)

averages_group = calc_avgs_per_testgroup_dpe(dfp_kpis, ["group"])

# %%
# DPE results - percentage kppis
rename = {
    "ctr": "ctr on DPE",
    "cstr_imp": "impression to content start rate on DPE",
    "cstr_click": "click to content start rate on DPE",
    "pct_users_with_click": "% of users with click on DPE",
    "pct_users_with_content_start": "% of users with content start from DPE",
}
df_pcts_user_usage = create_results_df(
    averages_group=averages_group,
    samples_control=samples_control,
    samples_test=samples_test,
    rename=rename,
    is_percentage=True,
)
df_pcts_user_usage.index.name = "percentages of"
df_pcts_user_usage

# %%
rename = {
    "clicks": "clicks on DPE",
    "impressions": " DPE impressions",
    "content_starts": "content starts from DPE",
    "unique_formats_shown": "number of different formats shown on DPE",
    "unique_formats_clicked": "number of different formats clicked on DPE",
    "unique_formats_started": "number of different formats started on DPE",
}
df_user_usage = create_results_df(
    averages_group=averages_group,
    samples_control=samples_control,
    samples_test=samples_test,
    rename=rename,
    is_percentage=False,
)
df_user_usage

# %%
samples_control_home = calc_metrics_samples(
    df=dfp_kpis_home_filter[dfp_kpis_home_filter.group == "control"],
    n=1000,
    metrics_function=calc_avgs_per_testgroup_home,
    fraction=0.111,  # so that samples have the same size of test group
)

samples_test_home = calc_metrics_samples(
    df=dfp_kpis_home_filter[dfp_kpis_home_filter.group == "test"],
    n=1000,
    metrics_function=calc_avgs_per_testgroup_home,
    fraction=1.0,
    with_replacement=True,
)

averages_group_home = calc_avgs_per_testgroup_home(dfp_kpis_home_filter, ["group"])

# %%
# DPE results - percentage kppis
rename = {
    "ctr": "ctr on Home",
    "cstr_imp": "impression to content start rate on Home",
    "cstr_click": "click to content start rate on Home",
    "pct_users_with_click": "% of users with click on Home",
    "pct_users_with_content_start": "% of users with content start from Home",
}
df_pcts_user_usage = create_results_df(
    averages_group=averages_group_home,
    samples_control=samples_control_home,
    samples_test=samples_test_home,
    rename=rename,
    is_percentage=True,
)
df_pcts_user_usage.index.name = "percentages of"
df_pcts_user_usage

# %%
rename = {
    "clicks": "clicks on Home",
    "impressions": " Home impressions",
    "content_starts": "content starts from Home",
    "unique_formats_shown": "number of different formats shown on Home",
    "unique_formats_clicked": "number of different formats clicked on Home",
    "unique_formats_started": "number of different formats started on Home",
}
df_user_usage = create_results_df(
    averages_group=averages_group_home,
    samples_control=samples_control_home,
    samples_test=samples_test_home,
    rename=rename,
    is_percentage=False,
)
df_user_usage

# %%
