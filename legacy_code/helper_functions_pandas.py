# %%
"""Helper functions.
Make sure you run this before running the other scripts
"""
import pandas as pd
import numpy as np
import plotly.express as px
import scipy.stats as scs
import plotly.graph_objects as go
from tqdm import tqdm

identifier = "profile_id"

control_color = "rgb(0, 134, 154, 0.5)"
test_color = "rgb(229, 0, 100, 0.5)"


def calc_avgs_per_testgroup_dpe(df, group_keys):
    return (
        df.groupby(group_keys, as_index=False)
        .agg(
            clicks=("clicks", "mean"),
            impressions=("impressions", "mean"),
            content_starts=("content_starts", "mean"),
            unique_formats_shown=("n_formats_shown", "mean"),
            unique_formats_clicked=("n_formats_clicked", "mean"),
            unique_formats_started=("n_formats_started", "mean"),
            pct_users_with_click=("clicks", lambda x: (x > 0).sum() / x.count()),
            pct_users_with_content_start=(
                "content_starts",
                lambda x: (x > 0).sum() / x.count(),
            ),
        )
        .assign(
            ctr=lambda x: x.clicks / x.impressions,
            cstr_imp=lambda x: x.content_starts / x.impressions,
            cstr_click=lambda x: x.content_starts / x.clicks,
        )
    )


def calc_avgs_per_testgroup_home(df, group_keys):
    return (
        df.groupby(group_keys, as_index=False)
        .agg(
            clicks=("clicks", "mean"),
            impressions=("impressions", "mean"),
            content_starts=("content_starts", "mean"),
            unique_formats_shown=("n_formats_shown", "mean"),
            unique_formats_clicked=("n_formats_clicked", "mean"),
            unique_formats_started=("n_formats_started", "mean"),
            pct_users_with_click=("clicks", lambda x: (x > 0).sum() / x.count()),
            pct_users_with_content_start=(
                "content_starts",
                lambda x: (x > 0).sum() / x.count(),
            ),
            content_starts_overall=("n_content_starts_aff", "mean"),
            unique_formats_started_overall=("n_unique_formats_aff", "mean"),
        )
        .assign(
            ctr=lambda x: x.clicks / x.impressions,
            cstr_imp=lambda x: x.content_starts / x.impressions,
            cstr_click=lambda x: x.content_starts / x.clicks,
        )
    )


def calc_avgs_per_testgroup_new_format_watched(df, group_keys):
    return df.groupby(group_keys, as_index=False).agg(
        avg_new_formats=("n_new", "mean"),
    )


def calc_metrics_samples(df, n, metrics_function, with_replacement=False, fraction=0.1):
    """Creates bootstrap (with replacemetn and fraction=1) or permutation samples and calculates KPIs for each sample.
    This is the pandas version and was used to do the statistical test on the CTR values.

    :param df: dataframe with the KPI values per user
    :type df: pandas.DataFrame
    :param n: number of samples to take
    :type n: int
    :param metrics_function: function that calculates metrics on each sample
    :type metrics_function: function
    :param with_replacement: samples are taken with or without replacement, defaults to False
    :type with_replacement: bool, optional
    :param fraction: fraction of users to take, defaults to 0.1
    :type fraction: float, optional
    :return: dataframe with the calculated KPI values for each of the n samples
    :rtype: pandas.DataFrame
    """
    df_all = pd.DataFrame()
    for _ in tqdm(range(n)):
        df_sample = df.sample(frac=fraction, replace=with_replacement)
        df_avgs = metrics_function(df_sample, group_keys=["group"])
        df_all = df_all.append(df_avgs, ignore_index=True)
    return df_all


def get_normal_curve(df, metric):
    """Fits a normal curve that is to be displayed in the distribution plots
    The confidence interval range is used to calculate the p-value

    :param df: dataframe with metric values for the samples (permutation or bootstrap)
    :type df: pandas.DataFrame
    :param metric: metric to be fitted
    :type metric: str
    :return: some parameters of the fitted normal
    :rtype: Tuple[np.Array, np.Array, float, float, float, float]
    """
    mean_dist, std_dist = scs.norm.fit(df[metric].astype("float").values)
    min_x = df[metric].min()
    max_x = df[metric].max()

    x = np.linspace(min_x, max_x, 50)
    pdf = scs.norm.pdf(x, loc=mean_dist, scale=std_dist)
    lcv, ucv = scs.norm.interval(0.95, loc=mean_dist, scale=std_dist)
    return x, pdf, lcv, ucv, mean_dist, std_dist


def calculate_p_and_power(metric, df_avgs_control, df_avgs_test, avg_test):
    """Calculate p-value and power

    :param metric: metric to be calculated
    :type metric: str
    :param df_avgs_control: sample averages in control group
    :type df_avgs_control: pandas.DataFrame
    :param df_avgs_test: sample averages in test group
    :type df_avgs_test: pandas.DataFrame
    :param avg_test: KPI total averages in test group
    :type avg_test: pandas.Series or pyspark.sql.Row
    :return: p-value and power
    :rtype: Tuple[float, float]
    """

    (
        x_control,
        pdf_control,
        lower_cf95,
        upper_cf95,
        mean_control,
        std_control,
    ) = get_normal_curve(df_avgs_control, metric)

    (x_test, pdf_test, _, _, mean_test, std_test) = get_normal_curve(
        df_avgs_test, metric
    )

    if avg_test[metric] < mean_control:
        p_value = scs.norm.cdf(avg_test[metric], mean_control, std_control)
        power = scs.norm.cdf(lower_cf95, mean_test, std_test)
    else:
        p_value = 1 - scs.norm.cdf(avg_test[metric], mean_control, std_control)
        power = 1 - scs.norm.cdf(upper_cf95, mean_test, std_test)

    # need to multiply p by 2 because it's a two sided test
    return 2 * p_value, power


def create_results_df(
    averages_group, samples_control, samples_test, rename, is_percentage=False
):
    """Create the table for summary of statistic significance

    :param averages_group: averages per group
    :type averages_group: pandas.DataFrame
    :param samples_control: averages for each sample of the control group
    :type samples_control: pandas.DataFrame
    :param samples_test: averages for each sample of the test group
    :type samples_test: pandas.DataFrame
    :param rename: dictionary to rename KPIs
    :type rename: Dict[str:str]
    :param is_percentage: is the table about percentages already, defaults to False
    :type is_percentage: bool, optional
    :return: p-value and power
    :rtype: Tuple[float, float]

    """
    # calculate averages
    averages_group_reduced = averages_group[list(rename.keys()) + ["group"]].copy()
    avg_test = (
        averages_group_reduced[averages_group_reduced.group == "test"]
        .set_index("group")
        .T.to_dict()["test"]
    )
    averages_group_reduced.group = averages_group_reduced.group.astype("str")
    df = averages_group_reduced.set_index("group").T

    if is_percentage:
        df["difference in %"] = df["test"] - df["control"]
        format_numbers = {
            "test": "{:.2%}",
            "control": "{:.2%}",
            "difference in %": "{:.2%}",
        }
    else:
        df["difference"] = df["test"] - df["control"]
        df["difference in %"] = (df["test"] - df["control"]) / df["control"]
        format_numbers = {"difference in %": "{:.2%}"}

    df["p-value"] = df.index.map(
        lambda x: calculate_p_and_power(x, samples_control, samples_test, avg_test)[0]
    )
    df["power"] = df.index.map(
        lambda x: calculate_p_and_power(x, samples_control, samples_test, avg_test)[1]
    )

    def color_p_value(val, p_value):
        color = "green" if val < p_value else "red"
        return "color: %s" % color

    return (
        df.rename(index=rename)
        .sort_values("p-value")
        .style.applymap(color_p_value, p_value=0.05, subset=["p-value"])
        .format("{:.3}")
        .format(format_numbers)
        .bar(subset=["difference in %"], align="zero", color=["#d65f5f", "#5fba7d"])
    )


def change_legend_names(df):
    df.rename(columns={"is_test_user": "group"}, inplace=True)
    df.replace({"group": {0: "control", 1: "test"}}, inplace=True)
    return df


# %%
