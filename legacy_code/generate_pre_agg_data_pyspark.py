"""This script has code to create a pre-aggregation data
on a user, platform, TR, day level.
You need to run the helper_functions_pyspark.py cells
in the interactive window first
The first import is just to get pylance to recognize the
functions and be able to switch to them with Cmd+click
"""
# %%
from .helper_functions_pyspark import get_and_aggregate_data  # noqa ignore=F405
from pyspark.sql import SparkSession, Window, functions as func

# %%
from datetime import date

# %%
# spark = SparkSession.builder.enableHiveSupport().getOrCreate()
start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)
bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"
# %%
# get the data from ga
df_all = get_and_aggregate_data(start_date=start_date, end_date=end_date)

# %%
# save on s3
df_all.write.partitionBy("date").mode("overwrite").parquet(
    f"s3://{bucket}/{path}/agg_data_user_platform_TR"
)

# %%
