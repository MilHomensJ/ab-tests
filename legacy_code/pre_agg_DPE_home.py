"""This script has code to create a pre-aggregated tables
for the DPE teaser row and for the home as a whole.
These tables will be used to generate the subsamples to calculate significance
You need to run the helper_functions_pyspark.py cells
in the interactive window first and the pre-aggregated table must exist.
The first import is just to get pylance to recognize the
functions and be able to switch to them with Cmd+click"""
# %%
# no need to run this cell - it's here to allow us to go to the function with cmd+click
from .helper_functions_pyspark import (
    agg_metrics_dpe_overall,
    get_content_starts_from_affinities,
    agg_metrics_home,
)  # noqa ignore=F405
from pyspark.sql import SparkSession

spark = SparkSession.builder.enableHiveSupport().getOrCreate()

# %%
from datetime import date

# %%
start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)
bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"

# %%
# read pre-aggregated data from s3
df_all = spark.read.parquet(f"s3://{bucket}/{path}/agg_data_user_platform_TR").cache()

# %%
# ************************** DPE metrics *********
df_kpis_user_dpe = agg_metrics_dpe_overall(
    df_all,
    group_keys=["profile_id", "is_test_user", "is_premium_user", "is_login_user"],
)

df_kpis_user_dpe.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/pre_agg_data/kpis_dpe_user.parquet"
)

df_kpis_user_dpe_plat = agg_metrics_dpe_overall(
    df_all,
    group_keys=[
        "profile_id",
        "is_test_user",
        "platform",
        "is_premium_user",
        "is_login_user",
    ],
)

df_kpis_user_dpe_plat.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/pre_agg_data/kpis_dpe_user_plat.parquet"
)
# %%

# ********************************** Homepage performance ******************
# content-starts global (from affinities table)
df_formats_started_affinities = get_content_starts_from_affinities(
    df_all, start_date, end_date
)


df_kpis_home_user = agg_metrics_home(
    df_all,
    group_keys=["profile_id", "is_test_user", "is_premium_user", "is_login_user"],
)


df_kpis_home_user = df_kpis_home_user.join(
    df_formats_started_affinities, on=["profile_id", "is_test_user"], how="left"
)

# %%
df_kpis_home_user.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/pre_agg_data/kpis_home_user.parquet"
)

# %%
df_kpis_user_dpe = agg_metrics_dpe_overall(
    df_all, group_keys=["profile_id", "is_test_user", "date"]
)
df_pkis_group = df_kpis_user_dpe.groupby("is_test_user", "date").agg(
    func.count("*").alias("n_users"),
    func.mean("clicks").alias("clicks"),
    func.mean("impressions").alias("impressions"),
    func.mean("content_starts").alias("content_starts"),
    func.mean("n_formats_shown").alias("formats_shown"),
    func.mean("n_formats_clicked").alias("formats_clicked"),
    func.mean("n_formats_started").alias("formats_started"),
)
# %%
df_users = (
    df_all.filter(func.col("teaser_row").startswith("Weil du"))
    .select("profile_id")
    .distinct()
)
df_kpis_home_user = agg_metrics_home(
    df_all.join(df_users, on="profile_id", how="right"),
    group_keys=["profile_id", "is_test_user"],
)

# %%
df_pkis_home = df_kpis_home_user.groupby("is_test_user").agg(
    func.approx_count_distinct("profile_id").alias("n_users"),
    func.mean("clicks").alias("clicks"),
    func.mean("impressions").alias("impressions"),
    func.mean("content_starts").alias("content_starts"),
    func.mean("n_formats_shown").alias("formats_shown"),
    func.mean("n_formats_clicked").alias("formats_clicked"),
    func.mean("n_formats_started").alias("formats_started"),
)
# %%
