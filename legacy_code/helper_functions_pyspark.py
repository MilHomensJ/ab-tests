"""Helper functions.
Make sure you run this before running the other scripts
"""
# %%
from labutils.pyspark.loader import custom_table_loader as ctl
from pyspark.sql import SparkSession, Window, functions as func
from pyspark.sql.types import IntegerType, LongType


spark = SparkSession.builder.enableHiveSupport().getOrCreate()
identifier = "profile_id"
AM_DB = "general_purpose_recommender"
AM_TABLE = "user_format_reco_recommendation_logs"
ANALYTICS_DB = "analytics"
COMBINED_USER_FORMAT_INTERACTIONS_TABLE = "affinities"
PROFILES_DB = "streaming"
PROFILES_TABLE = "account_profile_mapping_raw"


def absolute_ga_hit_time():
    """
    Return absolut hit time for GA dataframe in seconds.
    :return: column with the hit time
    """
    return (
        (func.col("visitStartTime") + func.col("hit_time") / 1000)
        .cast(LongType())
        .alias("absolute_hit_time")
    )


def consolidate_id_column(columns, name):
    return func.array_remove(
        func.array(
            *[
                func.when(func.length(func.col(c)) > 2, func.col(c)).otherwise(
                    func.lit("na")
                )
                for c in columns
            ]
        ),
        "na",
    )[0].alias(name)


def profile_personalization_id(
    columns=[
        "customdimension_index124",
        "hit_customdimension_index124",
        "customdimension_index21",
        "hit_customdimension_index21",
    ]
):
    """Create a column 'profile_personalization_id' with the content of either
    'customdimension_index124' if set or 'hit_customdimension_index124' when a user profile is used.
    Will contain null if all columns are set to 'na' or null.
    Could be combined with condition `is_login_user()` to
    ensure a profile_personalization_id exists.
    :param columns: optiona parameter to define the columns and order of columns to check for None or "na" values
    :return: profile_personalization_id column
    :rtype: pyspark.sql.Column
    """
    return consolidate_id_column(columns, "profile_personalization_id")


def is_login_user():
    """
    Return a boolean column that checks if any of the columns 'customdimension_index124' or 'hit_customdimension_index124' contains a valid profile_personalization_id
    or as fallback the columns 'customdimension_index21' and 'hit_customdimension_index21' contain a valid account_personalization_id (default profile).
    :return: Column object with the described behavior
    :rtype: pyspark.sql.Column
    """
    return (
        func.length(
            func.concat(
                func.coalesce(func.col("customdimension_index124"), func.lit("")),
                func.coalesce(func.col("hit_customdimension_index124"), func.lit("")),
                func.coalesce(func.col("customdimension_index21"), func.lit("")),
                func.coalesce(func.col("hit_customdimension_index21"), func.lit("")),
            )
        )
        > func.lit(8)
    ).alias("is_login_user")


def is_premium_user():
    """
    Return a boolean column that checks if any of the columns 'customdimension_index124' or 'hit_customdimension_index124' contains a valid profile_personalization_id
    or as fallback the columns 'customdimension_index21' and 'hit_customdimension_index21' contain a valid account_personalization_id (default profile).
    :return: Column object with the described behavior
    :rtype: pyspark.sql.Column
    """
    return (func.col("hit_customdimension_index10") != "free").alias("is_premium_user")


def between_dates(start_date, end_date):
    """Returns a column that mirrors this SQL expression
    `CONCAT(year, '-', month, '-', day) BETWEEN start_date and end_date`.

    :param start_date: The first date you want to load data for
    :type start_date: datetime.date or datetime.datetime
    :param end_date: The last date you want to load data for
    :type end_date: datetime.date or datetime.datetime
    :return: A column object that can be used as a filter to load
    a specific timeframe of ga_hits data.
    :rtype: pyspark.sql.Column
    """
    start = start_date.strftime("%Y%m%d")
    end = end_date.strftime("%Y%m%d")
    return func.concat(func.col("year"), func.col("month"), func.col("day")).between(
        start, end
    )


def get_dataset_between_dates(start_date, end_date):
    """get ga hits between the given period for the users.
    The columns in df_feature are also kept.
    Uses "ga.ga_hits" table as data source

    :param start_date: start of the period
    :type start_date: datetime.date
    :param end_date: end of the period
    :type end_date: datetime.date
    :param df_feature: dataframe with users for which billwerk data are needed
    :type df_feature: pyspark.sql.dataframe
    :return: dataframe with the ga data and features from df_feature (inner) joined on the identifier
    :rtype: pyspark.sql.dataframe
    """
    conditions = [
        (func.col("hit_eventinfo_eventcategory") == "videomessung")
        | (
            (func.col("hit_eventinfo_eventcategory") == "Enhanced Ecommerce")
            & (
                (
                    func.col("hit_page_pagepath").isin(
                        "www.tvnow.de/", "www.tvnow.at/", "www.tvnow.ch/"
                    )
                )
                | (func.col("hit_appinfo_screenname") == "/home")
                | (
                    func.regexp_extract(
                        func.col("hit_appinfo_screenname"),
                        r"(\/?tv\_now\_\w+\/home)$",
                        0,
                    )
                    != ""
                )
            )
        ),
        # func.col("hit_eventInfo_eventAction").isin(
        #     ["content", "Product Click", "Product Impression", "Product Impressions"]
        # ),  # Maybe redundancy (just used to filter data first)
        func.concat(func.col("year"), func.col("month"), func.col("day")).between(
            start_date.strftime("%Y%m%d"), end_date.strftime("%Y%m%d")
        ),
    ]
    columns = [
        is_premium_user(),
        is_login_user(),
        profile_personalization_id().alias("profile_id"),
        func.col("hit_eventinfo_eventcategory").alias("event_category"),
        func.col("hit_eventinfo_eventlabel").alias("event_label"),
        func.col("hit_customdimension_index24").alias("format"),
        func.col("platform"),
        func.col("visitid"),
        absolute_ga_hit_time(),
        func.to_date(func.col("date"), "yyyyMMdd").alias("date"),
        func.explode_outer("hit_product").alias("hit_product"),
    ]
    df_ga = ctl.load_table(
        spark, database="ga", table="ga_hits", columns=columns, conditions=conditions
    )

    return df_ga


def aggregate_metrics_ctr(group_cols, impression_and_clicks):
    """Aggregate metrics per bucket. Metrics are: clicks, teaser row impressions, number of users, set of formats shown and set of formats clicked

    :param group_cols: dimensions that define the buckets
    :type group_cols: List[str]
    :param impression_and_clicks: dataframe with impressios and clicks (and respective formats and teaser rows)
    :type impression_and_clicks: pyspark.sql.dataframe.DataFrame
    :return: aggregated dataframe
    :rtype: pyspark.sql.dataframe.DataFrame
    """
    df_click = (
        impression_and_clicks.filter(func.col("is_click"))
        .groupBy(group_cols)
        .agg(
            func.count(func.col("is_click")).alias("n_clicks"),
            func.collect_set(func.col("format")).alias("formats_clicked"),
        )
    )

    df_impression = impression_and_clicks.groupBy(group_cols).agg(
        func.sum(func.col("is_impression").cast(IntegerType())).alias("n_impressions")
    )

    df_formats_shown = (
        impression_and_clicks.filter(func.col("is_impression"))
        .groupBy(group_cols)
        .agg(func.collect_set(func.col("format")).alias("formats_shown"))
    )

    df_all = (
        df_impression.join(df_click, on=group_cols, how="outer")
        .join(df_formats_shown, on=group_cols, how="outer")
        .na.fill(0, ["n_impressions", "n_clicks", "formats_shown", "formats_clicked"])
    )
    return df_all


def aggregate_metrics_csr(click_window, group_cols, df):
    """Aggregate content start related metric: content_starts

    :param click_window: window function for later groupby
    :type click_window: window function
    :param df: dataframe with clicks, content_start per sesseion
    :type df: pyspark.sql.dataframe.DataFrame
    """
    df_previous_click = (
        df.withColumn("event_after", func.lead("event_label", 1).over(click_window))
        .withColumn("format_after", func.lead("format", 1).over(click_window))
        .withColumn(
            "content_eq_click",
            (func.col("format_after") == func.col("format_clicked")).cast(
                IntegerType()
            ),
        )
        .filter(
            "event_label == 'click'"
        )  # filter out the case that content-start to content-start
    )

    df_csr = (
        df_previous_click.groupby(group_cols)
        .agg(
            # func.sum("is_click").alias("n_clicks"), # Just used for checking if it's same as n_clicks in impressions_and_clicks
            func.sum(func.col("content_eq_click")).alias("n_starts"),
            func.collect_set(func.col("format_after")).alias("formats_started"),
        )
        .na.fill(0, ["content_starts"])
    )
    return df_csr


def get_and_aggregate_data(start_date, end_date):
    """Generate aggregated dataframe for impressions, clicks, content starts and user label ("is_test_use")

    :param start_date: start of the period
    :type start_date: datetime.date
    :param end_date: end of the period
    :type end_date: datetime.date
    :return: Aggregated dataframe with [identifier, "date", "platform", "module_position", "teaser_row"] and also different KPIs like impressions
    :rtype: pyspark.sql.dataframe.DataFrame
    """
    df_ga = get_dataset_between_dates(start_date, end_date)

    # remove kids profiles
    df_kids = load_kids_profiles()
    df_ga = df_ga.join(df_kids, on=identifier, how="left_anti")

    impression_and_clicks = (
        (
            df_ga.filter("event_category == 'Enhanced Ecommerce'")
            .select(
                "is_premium_user",
                "is_login_user",
                identifier,
                "date",
                "platform",
                func.col("hit_product")
                .getField("productListPosition")
                .alias("product_list_position"),
                func.col("hit_product").getField("isClick").alias("is_click"),
                func.col("hit_product").getField("isImpression").alias("is_impression"),
                func.col("hit_product").getField("productBrand").alias("format"),
                func.col("hit_product").getField("productListName").alias("teaser_row"),
                func.map_from_entries(
                    func.col("hit_product").getField("customDimensions")
                )[17].alias("product_list_id"),
                func.map_from_entries(
                    func.col("hit_product").getField("customDimensions")
                )[18].alias("module_position"),
            )
            .na.fill(False, ["is_impression", "is_click"])
        )
        #        .filter(func.col("module_position") < 2)
        .cache()
    )

    df_event = (
        df_ga.select(
            "is_premium_user",
            "is_login_user",
            identifier,
            "visitid",
            "date",
            "platform",
            "absolute_hit_time",
            "event_label",
            "format",
            func.col("hit_product")
            .getField("isClick")
            .alias("is_click")
            .cast(IntegerType()),
            func.col("hit_product").getField("productBrand").alias("format_clicked"),
            func.col("hit_product").getField("productListName").alias("teaser_row"),
            func.map_from_entries(func.col("hit_product").getField("customDimensions"))[
                18
            ].alias("module_position"),
        )
        .filter(
            (func.col("is_click") == 1) | (func.col("event_label") == "content-start")
        )
        .na.fill("click", subset=["event_label"])
    ).cache()

    # Load highlight_recommendation_logs
    df_user_groups_all = load_groups()

    df_user_groups = df_user_groups_all.select(identifier).distinct()
    df_user_groups = df_user_groups.withColumn("is_test_user", func.lit(1))

    # ***************************  Create impressions and clicks **************
    group_cols = [
        identifier,
        "date",
        "platform",
        "module_position",
        "teaser_row",
        "is_premium_user",
        "is_login_user",
    ]

    df_ctr = aggregate_metrics_ctr(group_cols, impression_and_clicks).cache()
    # *************************** Create content starts ***********************
    click_window = Window.partitionBy(identifier, "date", "visitid").orderBy(
        "absolute_hit_time"
    )

    df_csr = aggregate_metrics_csr(click_window, group_cols, df_event)
    # Join df_csr and df_ctr together
    return (
        df_ctr.join(df_csr, on=group_cols, how="left")
        .join(df_user_groups, on=identifier, how="left")
        .fillna(0, subset=["is_test_user", "n_starts"])
        .cache()
    )


def load_groups():
    return ctl.load_table(
        spark,
        database=AM_DB,
        table=AM_TABLE,
        columns=[func.col("user_id").alias(identifier)],
    ).filter(func.col("reco_type") == "formats-test")


def load_kids_profiles():
    return ctl.load_table(
        spark,
        database=PROFILES_DB,
        table=PROFILES_TABLE,
        columns=[func.col("profile_personalization_id").alias(identifier)],
    ).filter(func.col("profile_type") == "kids")


def load_affinities(from_date, to_date):
    """Loads affinities in the period defined for "premium" users, using content starts.

    :param from_date: start date
    :type from_date: datetime.date
    :param to_date: end date
    :type to_date: datetime.date
    :return: affinities table
    :rtype: pyspark.sql.dataframe.DataFrame
    """

    return (
        spark.table(f"{ANALYTICS_DB}.{COMBINED_USER_FORMAT_INTERACTIONS_TABLE}")
        .where(
            (
                func.col("day").between(
                    from_date.strftime("%Y-%m-%d"), to_date.strftime("%Y-%m-%d")
                )
            )
            & (func.col("affinity_type") == "video_starts")
            & (func.col("userstatus") == "premium")
        )
        .withColumnRenamed("id", identifier)
    )


def get_content_starts_from_affinities(df_all, start_date, end_date):
    """Join global content starts (from affinity table in athena) to previous df_all and generate "n_content_starts_aff" KPI.
    We needed to join the affinity table to have all the user with impressions represented (not only users with content starts)

    :param df_all: aggregated dataframe generated from function get_and_aggregate_data()
    :type df_all: pyspark.sql.dataframe.DataFrame
    :param start_date: start of the period
    :type start_date: datetime.date
    :param end_date: end of the period
    :type end_date: datetime.date
    :return: dataframe with additional KPI "n_content_starts_aff" for global content starts
    :rtype: pyspark.sql.dataframe.DataFrame
    """
    df_user_groups = df_all.select(identifier, "is_test_user").distinct()

    df_affinities = load_affinities(start_date, end_date).select(
        identifier, "format", "day", "affinity"
    )

    df_affinities = (
        df_affinities.join(df_user_groups, on=identifier, how="right")
        .fillna(0, subset="affinity")
        .cache()
    )

    df_formats_started = df_affinities.groupby(identifier, "is_test_user").agg(
        func.size(func.collect_set("format")).alias("n_unique_formats_aff"),
        func.sum("affinity").alias("n_content_starts_aff"),
    )
    return df_formats_started


def agg_metrics_dpe_overall(df_all, group_keys):
    return (
        df_all.filter(func.col("teaser_row").startswith("Weil du"))
        .groupby(*group_keys)
        .agg(
            func.sum("n_clicks").alias("clicks"),
            func.sum("n_impressions").alias("impressions"),
            func.sum("n_starts").alias("content_starts"),
            func.size(
                func.array_distinct(
                    func.flatten(func.collect_list(func.col("formats_shown")))
                )
            ).alias("n_formats_shown"),
            func.size(
                func.array_distinct(
                    func.flatten(func.collect_list(func.col("formats_clicked")))
                )
            ).alias("n_formats_clicked"),
            func.size(
                func.array_distinct(
                    func.flatten(func.collect_list(func.col("formats_started")))
                )
            ).alias("n_formats_started"),
            func.countDistinct("profile_id").alias("n_users"),
        )
        .withColumn("ctr", func.col("clicks") / func.col("impressions"))
        .withColumn("cstr_imp", func.col("content_starts") / func.col("impressions"))
        .withColumn("cstr_click", func.col("content_starts") / func.col("clicks"))
        .withColumn("clicks_user", func.col("clicks") / func.col("n_users"))
        .withColumn("impressions_user", func.col("impressions") / func.col("n_users"))
        .withColumn("cstarts_user", func.col("content_starts") / func.col("n_users"))
    )


def agg_metrics_home(df_all, group_keys):
    return df_all.groupby(*group_keys).agg(
        func.sum("n_clicks").alias("clicks"),
        func.sum("n_impressions").alias("impressions"),
        func.sum(
            func.when(
                # proxy for page visits - impressions on position one of the 2nd teaser row - avoids problems in aufmacher
                ((func.col("module_position") == 2)),
                func.col("n_impressions"),
            ).otherwise(0)
        ).alias("home_impressions"),
        func.sum("n_starts").alias("content_starts"),
        func.size(
            func.array_distinct(
                func.flatten(func.collect_list(func.col("formats_shown")))
            )
        ).alias("n_formats_shown"),
        func.size(
            func.array_distinct(
                func.flatten(func.collect_list(func.col("formats_clicked")))
            )
        ).alias("n_formats_clicked"),
        func.size(
            func.array_distinct(
                func.flatten(func.collect_list(func.col("formats_started")))
            )
        ).alias("n_formats_started"),
    )


# %%
