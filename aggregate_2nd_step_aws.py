from helper_functions_aws import effective_coverage, get_number_of_active_formats

from pyspark.sql import functions as func
from datetime import date
from pyspark.sql import SparkSession
import pandas as pd

# %%
spark = SparkSession.builder.enableHiveSupport().getOrCreate()

start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)
bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"

df_all = spark.read.parquet(f"s3://{bucket}/{path}/pre_agg_data").cache()

# %%
# content coverage "Weil du xyz..."
group_cols = ["user_id", "is_test_user", "is_premium_user", "is_login_user"]

kpi_aggs = [
    func.sum("n_clicks").alias("n_clicks"),
    func.sum("n_impressions").alias("n_impressions"),
    func.sum("content_start").alias("content_start"),
    func.sum("qualified_start").alias("qualified_start"),
    func.countDistinct(
        func.when(func.col("n_impressions") > 0, func.col("format_name")).otherwise(
            None
        )
    ).alias("n_formats_shown"),
    func.countDistinct(
        func.when(func.col("n_clicks") > 0, func.col("format_name")).otherwise(None)
    ).alias("n_formats_clicked"),
    func.countDistinct(
        func.when(func.col("content_start") > 0, func.col("format_name")).otherwise(
            None
        )
    ).alias("n_formats_started"),
    func.countDistinct(
        func.when(func.col("qualified_start") > 0, func.col("format_name")).otherwise(
            None
        )
    ).alias("n_formats_started_qlf"),
]

df_wdx_user = (
    df_all.filter(func.col("teaser_row").startswith("Weil du"))
    .groupby(*group_cols)
    .agg(*kpi_aggs)
)
# %%
df_wdx_user.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/agg_kpis/kpis_wdx_user.parquet"
)

# %% test users - only one evaluation with the whole group
group_cols = ["user_id", "is_test_user", "is_premium_user", "is_login_user", "platform"]
df_wdx_user_platform = (
    df_all.filter(func.col("teaser_row").startswith("Weil du"))
    .groupby(*group_cols)
    .agg(*kpi_aggs)
)
# %%
df_wdx_user_platform.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/agg_kpis/kpis_wdx_user_platform.parquet"
)

# %%
group_cols = ["user_id", "is_test_user", "is_premium_user", "is_login_user", "date"]
df_wdx_user_date = (
    df_all.filter(func.col("teaser_row").startswith("Weil du"))
    .groupby(*group_cols)
    .agg(*kpi_aggs)
)

df_wdx_user_date.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/agg_kpis/kpis_wdx_user_date.parquet"
)
# %%
# Home - we only consider users that saw the teaser row at least once
df_users = (
    df_all.filter(func.col("teaser_row").startswith("Weil du"))
    .select("user_id")
    .distinct()
)

group_cols = ["user_id", "is_test_user", "is_premium_user", "is_login_user"]
df_home_user = (
    df_all.join(df_users, on="user_id", how="inner").groupby(*group_cols).agg(*kpi_aggs)
)

df_home_user.write.mode("overwrite").parquet(
    f"s3://{bucket}/{path}/agg_kpis/kpis_home_user.parquet"
)

# %%
