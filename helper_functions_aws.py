# %%
from datetime import date
from itertools import chain
from typing import Dict, List

from pyspark.sql import DataFrame
from pyspark.sql import SparkSession, Window
from pyspark.sql import functions as func
from pyspark.sql.types import LongType, IntegerType

from labutils.pyspark.loader import custom_table_loader as ctl

identifier = "profile_personalization_id"
AM_DB = "general_purpose_recommender"
AM_TABLE = "user_format_reco_recommendation_logs"
PROFILES_DB = "streaming"
PROFILES_TABLE = "account_profile_mapping_raw"
TVNOW_DB = "tvnowdb"
FORMAT_TABLE = "vod_format"


def absolute_ga_hit_time():
    """
    Return absolut hit time for GA dataframe in seconds.
    :return: column with the hit time
    """
    return (
        (func.col("visitStartTime") + func.col("hit_time") / 1000)
        .cast(LongType())
        .alias("absolute_hit_time")
    )


def consolidate_id_column(columns, name):
    return func.array_remove(
        func.array(
            *[
                func.when(func.length(func.col(c)) > 2, func.col(c)).otherwise(
                    func.lit("na")
                )
                for c in columns
            ]
        ),
        "na",
    )[0].alias(name)


def profile_personalization_id(
    columns=[
        "customdimension_index124",
        "hit_customdimension_index124",
        "customdimension_index21",
        "hit_customdimension_index21",
    ]
):
    """Create a column 'profile_personalization_id' with the content of either
    'customdimension_index124' if set or 'hit_customdimension_index124' when a user profile is used.
    Will contain null if all columns are set to 'na' or null.
    Could be combined with condition `is_login_user()` to
    ensure a profile_personalization_id exists.
    :param columns: optiona parameter to define the columns and order of columns to check for None or "na" values
    :return: profile_personalization_id column
    :rtype: pyspark.sql.Column
    """
    return consolidate_id_column(columns, "profile_personalization_id")


def is_login_user():
    """
    Return a boolean column that checks if any of the columns 'customdimension_index124' or 'hit_customdimension_index124' contains a valid profile_personalization_id
    or as fallback the columns 'customdimension_index21' and 'hit_customdimension_index21' contain a valid account_personalization_id (default profile).
    :return: Column object with the described behavior
    :rtype: pyspark.sql.Column
    """
    return (
        func.length(
            func.concat(
                func.coalesce(func.col("customdimension_index124"), func.lit("")),
                func.coalesce(func.col("hit_customdimension_index124"), func.lit("")),
                func.coalesce(func.col("customdimension_index21"), func.lit("")),
                func.coalesce(func.col("hit_customdimension_index21"), func.lit("")),
            )
        )
        > func.lit(8)
    ).alias("is_login_user")


def is_premium_user():
    """
    Return a boolean column that checks if "hit_customdimension_index10" is not equal to "free"
    :return: Column object with the described behavior
    :rtype: pyspark.sql.Column
    """
    return (func.col("hit_customdimension_index10") != "free").alias("is_premium_user")


def calculate_clicks_and_impressions(
    exploded_ecommerce: DataFrame,
) -> DataFrame:
    """
    Calculate clicks and impressions by position and teaser row (id and name)
    :param exploded_ecommerce: Dataframe containing the exploded GA hits data with userstatus
    :return: Dataframe containing summarized clicks, impressions and formats per platform, module_position, userstatus and product_list_name
    """
    impression_and_clicks = exploded_ecommerce.select(
        func.col("hit_product").getField("productListName").alias("teaser_row"),
        # func.map_from_entries(func.col("hit_product").getField("customDimensions"))[
        #     18
        # ].alias("teaser_row_position"),
        func.col("hit_product").getField("productBrand").alias("format_name"),
        func.col("hit_product").getField("isClick").alias("is_click"),
        func.col("hit_product").getField("isImpression").alias("is_impression"),
        func.col("user_id"),
        func.col("visitid"),
    ).na.fill(False, ["is_impression", "is_click"])

    groupcols = [
        func.col("user_id"),
        func.col("visitid"),
        #        func.col("teaser_row_position"),
        func.col("teaser_row"),
        func.col("format_name"),
    ]

    n_clicks_impressions = impression_and_clicks.groupBy(groupcols).agg(
        func.sum(func.col("is_click").cast(IntegerType())).alias("n_clicks"),
        func.sum(func.col("is_impression").cast(IntegerType())).alias("n_impressions"),
    )

    return n_clicks_impressions


def check_qualified(video_events: DataFrame) -> DataFrame:
    """
    Calculate video through rates per user and format. Qualified content starts are limited at max 1 per visit (per user)
    :param video_events: Dataframe containing all video events (start, end, beat)
    :return: Dataframe containing video through rate per platform, user, session and format
    """
    grouping_keys = [
        "user_id",
        "visitid",
        "format_name",
        "video_id",
    ]

    video_events_clean = video_events.filter(
        func.col("format_name").isNull() | (func.col("format_name") != "na")
    )

    df_pre_affinity = video_events_clean.filter(
        (func.col("hit_eventinfo_eventlabel") == "content-start")
        & func.col("video_id").isNotNull()
        & func.col("video_length").isNotNull()
        & (func.col("video_length") >= 120)  # filter our trailers)
    )

    # Calculate the watch duration per video
    watch_durations = (
        video_events_clean.filter(
            func.col("hit_eventinfo_eventlabel").isin("content-beat", "content-ende")
            & func.col("video_id").isNotNull()
            & func.col("video_verweildauer").isNotNull()
        )
        .groupBy(*grouping_keys)
        .agg(
            func.sum("video_verweildauer").alias("video_verweildauer"),
        )
    )

    # Calculate the VTR per video and determine if start is qualified
    watch_durations_with_vl = (
        df_pre_affinity.groupBy(*grouping_keys)
        .agg(func.mean("video_length").alias("mean_video_length"))
        .join(
            watch_durations,
            on=grouping_keys,
            how="inner",
        )  # NOTE we are leaving rows with null VWD out so that there will be no divide by zero
        .withColumn(
            "qualified_start",
            func.when(
                (func.col("video_verweildauer") / func.col("mean_video_length")) > 0.15,
                1,
            ).otherwise(0),
        )
    )

    # Now aggregate on format level
    grouping_keys.remove("video_id")

    # Limit max vtr per video id to 1 and aggregate at format level
    return watch_durations_with_vl.groupby(*grouping_keys).agg(
        func.max("qualified_start").alias("qualified_start"),
    )


def explode_ecommerce(
    spark: SparkSession, start_date: date, end_date: date, page_conditions
) -> DataFrame:
    """Retrieve e-commerce data at the given date for a specific page and explode it to row format"""
    columns = [
        is_premium_user(),
        is_login_user(),
        profile_personalization_id(),
        absolute_ga_hit_time(),
        func.col("clientid"),
        func.col("visitid"),
        func.col("hit_product"),
        "platform",
        func.to_date(func.col("date"), "yyyyMMdd").alias("date"),
    ]
    conditions = page_conditions + [
        func.col("hit_eventinfo_eventcategory") == "Enhanced Ecommerce",
        between_dates(start_date, end_date),
    ]

    return (
        load_ga(spark, columns, conditions)
        .withColumn("hit_product", func.explode("hit_product"))
        .withColumn(
            "user_id",
            func.coalesce(func.col("profile_personalization_id"), func.col("clientid")),
        )
    )


def explode_ecommerce_from_home(
    spark: SparkSession, start_date: date, end_date: date
) -> DataFrame:
    """Retrieve e-commerce data at the given date from the homepage and explode it to row format"""
    conditions = [
        (
            func.col("hit_page_pagepath").isin(
                "www.tvnow.de/", "www.tvnow.at/", "www.tvnow.ch/"
            )
        )
        | (func.col("hit_appinfo_screenname") == "/home")
        | (
            func.regexp_extract(
                func.col("hit_appinfo_screenname"), r"(\/?tv\_now\_\w+\/home)$", 0
            )
            != ""
        ),
    ]
    return explode_ecommerce(spark, start_date, end_date, conditions)


def load_ga(spark, columns, conditions=[]):
    """Abstract the loading of ga_hits data.

    It uses the spark.sql api to load the data without relying on `SparkSession.sql(query)`
    and uses `SparkSession.table(table)` instead. `columns` has to be an array of `Column`
    objects or column names. `conditions` has to be an array of `and` conditions
    that are getting applied one after the other before being optimized by spark.

    Usage:

    .. code-block:: python

        columns = [
            ga.profile_personalization_id(),
            'clientid',
            func.when(func.col('hit_eventinfo_eventcategory') == func.lit('livestreammessung'), func.lit('live')).otherwise(func.lit('vod')),
        ]
        conditions = [
            func.col("hit_eventinfo_eventcategory") == func.lit("livestreammessung"),
            func.col('hit_eventinfo_eventlabel') != func.lit('content-ende'),
            "clientid == 'cookie'"
        ]
        df_out = ga.load_ga(spark_session, columns, conditions=conditions)

    :param spark: SparkSession Object
    :type spark: pyspark.sql.SparkSession
    :param columns: Array of columns to load from ga_hits
    :type columns: List[Union[pyspark.sql.Column, str]]
    :param conditions: List of conditions to apply while loading the dataframe.
        Each entry has to evaluate to a boolean, defaults to []
    :type conditions: List[Union[pyspark.sql.Column, str]], optional
    :return: ga_hits dataframe
    :rtype: pyspark.sql.DataFrame
    """
    df = spark.table("ga.ga_hits")

    for condition in conditions:
        df = df.where(condition)

    return df.select(*columns)


def get_video_events(
    spark: SparkSession, start_date: date, end_date: date
) -> DataFrame:
    """Retrieve the content starts at the given date"""
    columns = [
        profile_personalization_id(),
        absolute_ga_hit_time(),
        func.col("visitid"),
        func.col("clientid"),
        func.col("hit_customdimension_index24").alias("format_name"),
        func.col("hit_eventinfo_eventcategory"),
        func.col("hit_eventinfo_eventlabel"),
        func.col("hit_custommetric_index2").cast("int").alias("video_verweildauer"),
        func.col("hit_custommetric_index3").cast("int").alias("video_length"),
        func.col("hit_customdimension_index31").alias("video_id"),
    ]
    conditions = [
        func.col("hit_eventinfo_eventcategory") == "videomessung",
        func.col("hit_eventinfo_eventlabel").isin(
            "content-beat", "content-ende", "content-start"
        ),
        between_dates(start_date, end_date),
    ]

    return (
        load_ga(spark, columns, conditions)
        .withColumn(
            "user_id",
            func.coalesce(func.col("profile_personalization_id"), func.col("clientid")),
        )
        .drop("profile_personalization_id")
    )


def flatten_ga_hits(
    exploded_ecommerce: DataFrame, content_starts: DataFrame
) -> DataFrame:
    """Extract all necessary fields from exploded ecommerce data and content starts"""
    # return
    exploded_ga_hits = (
        exploded_ecommerce.withColumn(
            "hit_eventinfo_eventcategory", func.lit("Enhanced Ecommerce")
        )
        .withColumn("hit_eventinfo_eventlabel", func.lit(None))
        .withColumn("format_name", func.lit(None))
        .unionByName(content_starts.withColumn("hit_product", func.lit(None)))
    )

    return exploded_ga_hits.select(
        func.col("user_id"),
        func.when(
            func.col("hit_product").getField("isImpression"), func.lit("impression")
        )
        .otherwise(
            func.when(
                func.col("hit_product").getField("isClick"), func.lit("click")
            ).otherwise(func.lit("content-start"))
        )
        .alias("event_type"),
        func.col("hit_product").getField("productListName").alias("product_list_name"),
        # func.map_from_entries(func.col("hit_product").getField("customDimensions"))[18]
        # .cast("int")
        # .alias("module_position"),
        func.coalesce(
            func.col("hit_product").getField("productBrand"), func.col("format_name")
        ).alias("format_name"),
        func.col("visitid"),
        func.col("absolute_hit_time"),
    )


def match_previous_events(df_events: DataFrame) -> DataFrame:
    """Add previous events to a content start
    1. add the previous click event per profile, session and format if it exists,
    2. add the previous impression per profile, session, format and clicked teaser row if it exists.
    """
    click_window = Window.partitionBy("user_id", "visitid", "format_name").orderBy(
        "absolute_hit_time"
    )

    return (
        df_events.filter(func.col("event_type").isin("click", "content-start"))
        .withColumn("previous_event_type", func.lag("event_type", 1).over(click_window))
        # .withColumn(
        #     "module_position",
        #     func.lag("module_position", 1).over(
        #         click_window
        #     ),  # because content_starts do NOT have module_position (it comes from exploded hit_product)
        # )
        .withColumn(
            "teaser_row",
            func.lag("product_list_name", 1).over(click_window),
        )
        .filter(
            func.col("previous_event_type") == "click"
        )  # Only if click before the content_start NOTE even if something else happened meanwhile
        .filter(func.col("event_type") == "content-start")
        .withColumn("content_start", func.lit(1))
        .drop(
            "event_type",
            "previous_event_type",
            "product_list_name",
            "absolute_hit_time",
        )
    )


def between_dates(start_date, end_date):
    """Return a column that mirrors this SQL expression `CONCAT(year, '-', month, '-', day) BETWEEN start_date and end_date`.

    :param start_date: The first date you want to load data for
    :type start_date: datetime.date or datetime.datetime
    :param end_date: The last date you want to load data for
    :type end_date: datetime.date or datetime.datetime
    :return: A column object that can be used as a filter to load
        a specific timeframe of ga_hits data.
    :rtype: pyspark.sql.Column
    """
    start = start_date.strftime("%Y%m%d")
    end = end_date.strftime("%Y%m%d")
    return func.concat(func.col("year"), func.col("month"), func.col("day")).between(
        start, end
    )


def load_groups():
    return ctl.load_table(
        spark,
        database=AM_DB,
        table=AM_TABLE,
        columns=[func.col("user_id").alias(identifier)],
    ).filter(func.col("reco_type") == "formats-test")


def load_kids_profiles():
    return ctl.load_table(
        spark,
        database=PROFILES_DB,
        table=PROFILES_TABLE,
        columns=[func.col("profile_personalization_id").alias(identifier)],
    ).filter(func.col("profile_type") == "kids")


def users_with_status(exploded_ecommerce: DataFrame) -> DataFrame:
    df_user_groups_all = load_groups()
    df_user_groups = df_user_groups_all.select(identifier).distinct()
    df_user_groups = df_user_groups.withColumn("is_test_user", func.lit(1))

    df_kids = load_kids_profiles()
    return (
        exploded_ecommerce.join(df_kids, on=identifier, how="left_anti")
        .join(df_user_groups, on=identifier, how="left")
        .fillna(0, subset=["is_test_user"])
        .select(
            "is_premium_user",
            "is_login_user",
            "is_test_user",
            "platform",
            "visitid",
            "user_id",
        )
        .distinct()
    )


def get_number_of_active_formats(
    spark: SparkSession,
    start_date: date,
    end_date: date,
) -> int:

    columns = [
        "id",
        "format_name_lang",
        "onlinedate",
        "offlinedate",
        "menuonline",
        "menuoffline",
        "formatdisabled",
        "platform",
        "menustatus",
        "online_qc",
    ]
    df_vod_format = ctl.load_table(
        spark,
        database=TVNOW_DB,
        table=FORMAT_TABLE,
        columns=columns,
    )
    # convert datetime columns to utc and filter for active formats
    return df_vod_format.filter(
        (func.col("formatdisabled") == 0)
        & (func.lower(func.col("platform")).contains("tvnow"))
        & (func.lower(func.col("menustatus")) == "aktiv")
        & (func.col("online_qc") == 1)
        & (
            func.from_unixtime((func.col("onlinedate") / 1000).cast(IntegerType()))
            <= start_date
        )
        & (
            func.from_unixtime((func.col("offlinedate") / 1000).cast(IntegerType()))
            > end_date
        )
        & (
            func.from_unixtime((func.col("menuonline") / 1000).cast(IntegerType()))
            <= start_date
        )
        & (
            func.from_unixtime((func.col("menuoffline") / 1000).cast(IntegerType()))
            > end_date
        )
    ).count()


def effective_coverage(n_formats: int, df: DataFrame, col_name: str) -> float:
    """Get the effective coverage of the whole catologue.

    See Appendix A in Gomez-Uribe, C. A., & Hunt, N. (2016).
    The netflix recommender system: Algorithms, business value, and innovation for more information.

    .. math::

        (2 * Σ_{i=1}^N (p_i * i) - 1) / n_{catalogue}

    with number of recommended items N and:

    .. math::

        p_i >= p_{i+1}

    the probability/usage of item i, being i the ranking according to the metric in question (could be clicks,
    content starts, etc)

    :param formats: All formats in the catologue which can be recommended
    :type formats: pyspark.sql.DataFrame
    :return: effective catalogue coverage
    :rtype: float
    """

    normed_predictions = (
        df.withColumn(
            "total", func.sum(col_name).over(Window.partitionBy())
        ).withColumn(
            "rank", func.row_number().over(Window().orderBy(func.col(col_name).desc()))
        )
    ).select((func.col(col_name) / func.col("total") * func.col("rank")).alias("score"))

    return (
        2 * normed_predictions.agg(func.sum("score")).collect()[0][0] - 1
    ) / n_formats


# %%
