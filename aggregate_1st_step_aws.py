from helper_functions_aws import (
    explode_ecommerce_from_home,
    flatten_ga_hits,
    match_previous_events,
    get_video_events,
    check_qualified,
    users_with_status,
    calculate_clicks_and_impressions,
)
from pyspark.sql import functions as func
from datetime import date
from pyspark.sql import SparkSession

# %%
USER_SUBSCRIPTIONS_DB = "billwerk"
USER_SUBSCRIPTIONS_TABLE = "subscriptions"

spark = SparkSession.builder.enableHiveSupport().getOrCreate()

start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)
bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"

# %%
# Load impressions and clicks from GA
exploded_ecommerce = explode_ecommerce_from_home(spark, start_date, end_date).cache()

# %%
# get first date of the visit in case of visits that span more than 1 day (e.g start at 23:59)
sessions = (
    exploded_ecommerce.select("user_id", "date", "visitid")
    .distinct()
    .groupby("visitid", "user_id")
    .agg(func.min("date").alias("date"))
)

# %%
df_users = users_with_status(exploded_ecommerce)

# %%
# Load content starts and view time
video_events = get_video_events(spark, start_date, end_date)

content_starts = video_events.filter(
    func.col("hit_eventinfo_eventlabel") == "content-start"
).drop("video_verweildauer", "video_length", "video_id")

# calculate qualified content starts per visit, user
qualified_cstarts = check_qualified(video_events)

# %%
matched_content_starts = match_previous_events(
    flatten_ga_hits(
        exploded_ecommerce.drop(
            "is_premium_user",
            "is_login_user",
            "profile_personalization_id",
            "platform",
            "date",
        ),
        content_starts,
    )
)

matched_qualified_content_starts = matched_content_starts.join(
    qualified_cstarts,
    on=[
        "user_id",
        "format_name",
        "visitid",
    ],
    how="left",
)


# %%
# calculate aggregations
df_clicks_impressions = calculate_clicks_and_impressions(exploded_ecommerce)


# %%
df_all_visit = (
    df_clicks_impressions.join(
        matched_qualified_content_starts,
        on=["user_id", "visitid", "format_name", "teaser_row"],
        how="left",
    )
    .na.fill(0, ["content_start", "qualified_start"])
    .join(df_users, on=["user_id", "visitid"])
    .join(sessions, on=["user_id", "visitid"])
)

# %%
# loose visit id
df_all = df_all_visit.groupBy(
    "user_id",
    "format_name",
    "teaser_row",
    "date",
    "platform",
    "is_premium_user",
    "is_login_user",
    "is_test_user",
).agg(
    func.sum("n_clicks").alias("n_clicks"),
    func.sum("n_impressions").alias("n_impressions"),
    func.sum("content_start").alias("content_start"),
    func.sum("qualified_start").alias("qualified_start"),
)

# %%
df_all.write.partitionBy("date").mode("overwrite").parquet(
    f"s3://{bucket}/{path}/pre_agg_data"
)

# %%
