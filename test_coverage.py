from pyspark.sql.types import StringType, IntegerType, StructType, StructField
from pyspark.sql import Window, Row
from helper_functions_aws import effective_coverage

df = spark.createDataFrame(
    [
        Row("format1", 2),
        Row("format2", 2),
        Row("format3", 2),
        Row("format4", 2),
        Row("format5", 2),
        Row("format6", 2),
    ],
    StructType(
        [
            StructField("format", StringType()),
            StructField("n_views", IntegerType()),
        ]
    ),
)


# %%
df.withColumn("total", func.sum("n_views").over(Window.partitionBy())).withColumn(
    "rank", func.row_number().over(Window().orderBy(func.col("n_views").desc()))
).show()
# %%
effective_coverage(6, df, "n_views")
