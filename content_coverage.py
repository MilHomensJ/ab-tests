from helper_functions_aws import effective_coverage, get_number_of_active_formats

from pyspark.sql import functions as func
from datetime import date
from pyspark.sql import SparkSession
import pandas as pd

# %%
spark = SparkSession.builder.enableHiveSupport().getOrCreate()

start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)
bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"

df_all = spark.read.parquet(f"s3://{bucket}/{path}/pre_agg_data").cache()

# %%
# limit to premium now
df_all_premium = df_all.filter(
    (func.col("is_premium_user") == 1) & (func.col("is_login_user") == 1)
)

# %%
# nubmer of active formats in the test period
n_formats = get_number_of_active_formats(
    spark, start_date=start_date, end_date=end_date
)
# %%
# content coverage "Weil du xyz..."
group_cols = ["user_id", "is_test_user", "format_name"]

sum_aggs = [
    func.sum("n_clicks").alias("n_clicks"),
    func.sum("n_impressions").alias("n_impressions"),
    func.sum("content_start").alias("content_start"),
    func.sum("qualified_start").alias("qualified_start"),
]

df_wdx = (
    df_all_premium.filter(func.col("teaser_row").startswith("Weil du"))
    .groupby(*group_cols)
    .agg(*sum_aggs)
).cache()

# %% test users - only one evaluation with the whole group
kpis = ["n_clicks", "n_impressions", "content_start", "qualified_start"]
eff_cov_test_dict = {}
df_test = (
    df_wdx.filter(func.col("is_test_user") == 1).groupby("format_name").agg(*sum_aggs)
)

for kpi in kpis:
    coverage_test = effective_coverage(
        n_formats=n_formats,
        df=df_test,
        col_name=kpi,
    )
    eff_cov_test_dict[kpi] = coverage_test

eff_cov_test_dict["sample"] = 0
eff_cov_test_dict["group"] = "test"
# %%  control users -> many samples with the same size of the test group
df_test_users = (
    df_wdx.filter(func.col("is_test_user") == 1).select("user_id").distinct()
)

df_control_users = (
    df_wdx.filter(func.col("is_test_user") == 0).select("user_id").distinct()
)

fraction = df_test_users.count() / df_control_users.count()

# %%
n_samples = 100
list_dicts = []

for i in range(n_samples):
    df_control_users_sample = df_control_users.sample(fraction=fraction)

    eff_cov_control_dict = {}
    df_control_sample = (
        df_wdx.join(df_control_users_sample, on="user_id", how="inner")
        .groupby("format_name")
        .agg(*sum_aggs)
    )

    eff_cov_control_dict["sample"] = i
    eff_cov_control_dict["group"] = "control"
    for kpi in kpis:
        coverage_test = effective_coverage(
            n_formats=n_formats,
            df=df_control_sample,
            col_name=kpi,
        )
        eff_cov_control_dict[kpi] = coverage_test
        list_dicts.append(eff_cov_control_dict)


# %%
list_dicts.append(eff_cov_test_dict)

df = pd.DataFrame(list_dicts)
df.to_parquet(f"s3://{bucket}/{path}/coverage.parquet")
# %%
