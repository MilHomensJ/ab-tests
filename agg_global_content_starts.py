# %%
import awswrangler as wr
import boto3
from datetime import date

bucket = "dev-data-science-lab"
path = "recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus"
start_date = date(2021, 10, 22)
end_date = date(2021, 10, 28)

boto3_session = boto3.session.Session(profile_name="dev")
# %%


query = f"""
SELECT id as user_id,
         COUNT(DISTINCT format) as n_formats,
         id_type,
         userstatus,
         SUM(affinity) as content_starts
FROM "analytics"."combined_user_format_interactions"
WHERE day
    BETWEEN '2021-10-22'
        AND '2021-10-28'
        AND affinity_type='video_starts'
GROUP BY id,
         id_type,
         userstatus
"""

df = wr.athena.read_sql_query(query, database="analytics", boto3_session=boto3_session)

# %%
# get only users that actually saw the teaser row
filepath = "s3://dev-data-science-lab/recos/you_may_also_like_this/AB_Testing/Format_reco_rtlplus/agg_kpis/kpis_wdx_user.parquet/"
df_wdx = wr.s3.read_parquet(filepath, dataset=True, boto3_session=boto3_session)
# %%

df_global = df.merge(
    df_wdx[["user_id", "is_test_user", "is_premium_user", "is_login_user"]],
    on="user_id",
    how="inner",
)
# %%
# # check results
# df_compare = df_global.groupby("is_test_user").agg(
#     av_cont_start=("content_starts", "mean"),
#     av_n_formats=("n_formats", "mean"),
# )
# df_compare
# %%
wr.s3.to_parquet(
    df=df_global,
    path=f"s3://{bucket}/{path}/agg_kpis/kpis_global_user.parquet",
    boto3_session=boto3_session,
)

# %%
